$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "deploy_capistrano/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "deploy_capistrano"
  s.version     = DeployCapistrano::VERSION
  s.authors     = ['Brad Herring']
  s.email       = ['bradley.herring@cgifederal.com']
  s.summary     = "Deployments via Capistrano."
  s.description = "Creates modules and views to faciliate the deployment of files via Capistrano."

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 3.2.13"
  s.add_dependency "net-ssh"

  s.add_development_dependency "sqlite3"
end
