module Capistrano
  require 'net/ssh'

  def self.before(app_deployment, options = Hash.new)
  end

  def self.perform_deploy(app_deployment, options = Hash.new)
    resource = options[:resource]
    logger = options[:logger]

    Net::SSH.start(resource.capistrano_host, resource.capistrano_user) do |ssh|
      output = Array.new

      if resource.capistrano_command.is_a? Array
        resource.capistrano_command.each do |command|
          output << execute(ssh, command)
        end
      else
        output << execute(ssh, resource.capistrano_command)
      end

      errors = Array.new
      output.each do |command|
        #require 'pry'
        #binding.pry
        exit_code = command[:exit_code]

        if exit_code > 0
          errors << command[:stderr_data]
        end

        #Process Log entries
        command[:all_data].each do |entry|
          logger.debug entry
        end
      end

      raise errors.join.split('/')[0] if errors.count > 0
    end
  end

  def self.after(app_deployment, options = Hash.new)
    options[:logger].debug 'Capistrano - After'
  end

  def self.error(app_deployment, exception, options = Hash.new)
    options[:logger].debug 'Capistrano - Error'

  end

  def self.success(app_deployment, options = Hash.new)
    options[:logger].debug 'Capistrano - Success'
  end

  def self.failure(app_deployment, exception, options = Hash.new)
    options[:logger].debug 'Capistrano - Failure'
    options[:logger].error exception.message
  end

  private
  def self.execute(ssh, command)
    all_data = Array.new
    stdout_data = ""
    stderr_data = ""
    exit_code = nil
    exit_signal = nil
    ssh.open_channel do |channel|
      channel.exec(command) do |ch, success|
        unless success
          abort "FAILED: couldn't execute command (ssh.channel.exec)"
        end
        channel.on_data do |ch,data|
          stdout_data+=data
          all_data << data
        end

        channel.on_extended_data do |ch,type,data|
          stderr_data+=data
          all_data << data
        end

        channel.on_request("exit-status") do |ch,data|
          exit_code = data.read_long
        end

        channel.on_request("exit-signal") do |ch, data|
          exit_signal = data.read_long
        end
      end
    end
    ssh.loop
    { :all_data => all_data, :stdout_data => stdout_data, :stderr_data => stderr_data, :exit_code => exit_code, :exit_signal => exit_signal }
  end
end
