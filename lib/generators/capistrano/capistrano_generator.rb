class CapistranoGenerator < Rails::Generators::NamedBase
  include Rails::Generators::Migration
  source_root File.expand_path('../templates', __FILE__)

  VIEWS_DIRECTORY = Rails.root.join('app', 'views', 'capistrano')
  MODULE_DIRECTORY = Rails.root.join('lib', 'modules', 'deployment_methods')
  CSS_DIRECTORY = Rails.root.join('app', 'assets', 'stylesheets')


  def generate_views
    FileUtils.mkdir_p(VIEWS_DIRECTORY) unless File.directory?(VIEWS_DIRECTORY)
    copy_file '_status.html.erb', File.join(VIEWS_DIRECTORY, '_status.html.erb')
    copy_file '_form.html.erb', File.join(VIEWS_DIRECTORY, '_form.html.erb')
    template '_capistrano.html.erb', File.join(VIEWS_DIRECTORY, '_capistrano.html.erb')
    copy_file "capistrano.css.scss", File.join(CSS_DIRECTORY, 'capistrano.css.scss')
  end

  def generate_module
    capistrano_dir = File.join(MODULE_DIRECTORY, 'capistrano')
    FileUtils.mkdir_p(MODULE_DIRECTORY) unless File.directory?(MODULE_DIRECTORY)
    FileUtils.mkdir_p(capistrano_dir) unless File.directory?(capistrano_dir)

    copy_file File.join('capistrano', 'info.yml'), File.join(capistrano_dir, 'info.yml')
    copy_file File.join('capistrano', 'capistrano.rb'), File.join(capistrano_dir, 'capistrano.rb')
  end

  def create_migration_file_add_columns
    migration_template 'migrations/add_capistrano_columns.rb.erb', "db/migrate/add_capistrano_columns_to_#{name}.rb"
  end

  def create_migration_file_add_host
    migration_template 'migrations/add_capistrano_host_column.rb.erb', "db/migrate/add_capistrano_host_column_to_#{name}.rb"
  end

  def create_migration_file_add_user
    migration_template 'migrations/add_capistrano_user_column.rb.erb', "db/migrate/add_capistrano_user_column_to_#{name}.rb"
  end

  private
  def self.next_migration_number(path)
    unless @prev_migration_nr
      @prev_migration_nr = Time.now.utc.strftime("%Y%m%d%H%M%S").to_i
    else
      @prev_migration_nr += 1
    end
    @prev_migration_nr.to_s
  end
end
